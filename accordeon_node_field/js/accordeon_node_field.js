(function($) {
	Drupal.behaviors.YOURTHEMENAME = {
		attach : function(context, settings) {

			$('.trigger').not('.trigger_active').next('.toggle_container').hide();
			$('.trigger').click(function() {
				var trig = $(this);
				if (trig.hasClass('trigger_active')) {
					trig.next('.toggle_container').slideToggle('fast');
					trig.removeClass('trigger_active');
				} else {
					$('.trigger_active').next('.toggle_container').slideToggle('fast');
					$('.trigger_active').removeClass('trigger_active');
					trig.next('.toggle_container').slideToggle('fast');
					trig.addClass('trigger_active');
				};
				return false;
			});

		}
	};
})(jQuery);
